txtyellow='\[\033[1;33m\]'
#txtred='\[\033[1;31\]'
txtred='\[\033[38;5;160m\]'
txtblue='\[\033[1;34m\]'
txtreset='\[\033[0m\]'

export HISTCONTROL=ignoreboth:erasedups
export EDITOR=vim

# check if we're at a work system and if so set proxy
if [ $(hostname) == "md1w3rzc" ]; then
    export http_proxy="194.145.60.1:9400"
    export https_proxy="194.145.60.1:9400"

else
    export http_proxy=""
    export https_proxy=""
fi

# easy function to set/unset proxy
function proxy {

    if [[ -z $1 ]]; then
        echo "HTTP Proxy:  $http_proxy"
        echo "HTTPS Proxy: $https_proxy"
    
    elif [[ $1 == "on" ]]; then
        export http_proxy="194.145.60.1:9400"
        export https_proxy="194.145.60.1:9400"
        echo "Proxy set to"
        echo "http:  '$http_proxy'"
        echo "https: '$https_proxy'"

    elif [[ $1 == "off" ]]; then
        export http_proxy=""
        export https_proxy=""
        echo "Proxy unset"
        echo "http:  '$http_proxy'"
        echo "https: '$https_proxy'"

    else 
        echo "Invalid argument '$1'. Only 'on' or 'off'"
    
    fi
}

#update git repo if an interactive shell
if echo "$-" | grep i > /dev/null; then
    bash -c "cd ~/dotfiles; git pull > /dev/null &"
fi

# increase open file handle soft limit to hard limit
ulimit -Sn $(ulimit -Hn)
eval "$(dircolors -b ~/.dircolors)" 

##############
#  Functions #
##############

function dark {
    eval "$(dircolors -b ~/dotfiles/dircolors.ansi.solarized.dark)" 
    sed -i -E "s/(set background=)(.*)/\1dark/" ~/.vimrc
}

function light {
    eval "$(dircolors -b ~/dotfiles/dircolors.ansi.solarized.light)" 
    sed -i -E "s/(set background=)(.*)/\1light/" ~/.vimrc
}

function links {
    if [[ $1 == "" ]]; then
        cat /mnt/d/work/00_notes/links.md | cut -d "(" -f1
        return 0
    fi

    NAME=$(cat /mnt/d/work/00_notes/links.md | cut -d "(" -f1 | tr -d "*" | grep -iF $1)

    if [[ $NAME == "" ]]; then
        echo "Nothing found"
        
    else
        echo "Found $NAME"
        LINK=$(grep -iF "$NAME" /mnt/d/work/00_notes/links.md | cut -d "(" -f2- | tr -d ")")
        echo -n $LINK | clip.exe
    fi
}

function templates {
    if [[ $1 == "" ]]; then
        cat /mnt/d/work/00_notes/templates.md | cut -d "(" -f1
        return 0
    fi

    NAME=$(cat /mnt/d/work/00_notes/templates.md | cut -d "(" -f1 | tr -d "*" | grep -iF $1)

    if [[ $NAME == "" ]]; then
        echo "Nothing found"
        
    else
        echo "Found $NAME"
        LINK=$(grep -iF "$NAME" /mnt/d/work/00_notes/templates.md | cut -d "(" -f2 | tr -d ")")
        echo -n $LINK | clip.exe
    fi
}

##############
# SET PROMPT #
##############
#export PS1="\[\033[38;5;32m\]\u\[$(tput sgr0)\]\[\033[38;5;3m\]@\[$(tput sgr0)\]\[\033[38;5;74m\]\h\[$(tput sgr0)\]\[\033[38;5;3m\]:[\w]:\[$(tput sgr0)\]\[\033[38;5;15m\] \[$(tput sgr0)\]"
#export PS1="\[\033[38;5;32m\]\u\[\033[38;5;3m\]@\\[\033[38;5;74m\]\h\[\033[38;5;3m\]:[\w]:\[\033[38;5;15m\]"


# make root prompt red
if [[ $UID -eq 0 ]]; then
   export PS1="$txtred\u@\h:$txtyellow[\w]:$txtreset "
else
   # regular shell prompt
   export PS1="$txtblue\u$txtyellow@$txtblue\h:$txtyellow[\w]:$txtreset "
fi

# fix HOME/END problem in tmux
if [[ -n "$TMUX" ]]; then
    bind '"\e[1~":"\eOH"'
    bind '"\e[4~":"\eOF"'
fi

#########
# Alias #
#########
alias "ll=ls --color -lhv" #-v  is for version sort only on GNU systems
alias rm='rm -I'
alias python='python3'
alias ipython='ipython3'
alias pip='pip3'
alias kanban-up="ssh -fNTML 8080:localhost:80 blanka"
alias kanban-status="ssh -TO check blanka"
alias kanban-down="ssh -TO exit blanka"


alias nessus-up="ssh -fNTML 8834:localhost:8834 blanka"
alias nessus-status="ssh -TO check blanka"
alias nessus-down="ssh -TO exit blanka"

alias brave="/mnt/c/Program\ Files\ \(x86\)/BraveSoftware/Brave-Browser/Application/brave.exe"
alias telnet="telnet -e +"

# UTF8 Encoding as standard for tmux and force tmux to use 256 color terminal
alias tmux='tmux -u -2'

# make less interpret color codes
alias less='less -R'
alias doch='sudo $(history -p !-l)'

set 'bind bell-style none'

# start ssh agent and add key if no agent is already running
pgrep ssh-agent > /dev/null
if [ $? -eq 1 ]; then
    eval `ssh-agent`
    ssh-add ~/.ssh/id_rsa
fi

