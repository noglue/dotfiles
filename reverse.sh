#!/bin/bash
############################
# reverse.sh
# this script restores the original dotfiles from the host
############################

########## Variables

dir=~/dotfiles                    # dotfiles directory
olddir=$dir/dotfiles_old             # old dotfiles backup directory
files="bashrc dircolors vim vimrc profile inputrc tmux.conf"
##########

# move every replaced dotfile from $olddir to original location
for file in $files; do
    echo "removing symlinks"
    rm ~/.$file
    echo "restoring original file: $file"
    if [ -e $olddir/$file ]; then
	  cp $olddir/$file ~/.$file
    else
	echo "file: $file not found"
    fi
done
